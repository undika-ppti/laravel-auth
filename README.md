# Universitas Dinamika Laravel Authentication Module

Authentication module for Laravel using Universitas Dinamika auth routine.

## Compatible with

Laravel ^6|^7|^8|^9

## This Package Provide

1. `UserProvider` to handle authentication routine on Universitas Dinamika
1. `Civitas` model & its basic instance to hold attributes required for authentication.

## This Package Does Not Provide

1. Database connection to Universitas Dinamika database server
1. Controller nor view for authentication (login page, etc)
1. Automatic guard default change, it need to be adjusted manually

## Installation

1. Install this component via composer
1. Add/modify entry on `config.auth.provider`.
   1. Set driver to `civitas_undika`.
   1. Set model to instance of `\Undika\Auth\User\Civitas`. The example use `\Undika\Auth\User\Karyawan` which is pre-created model with basic functionality for authentication.

```php
...

'karyawan' => [
    'driver' => 'civitas_undika',
    'model' => \Undika\Auth\User\Karyawan::class,
],

...
```
