<?php

namespace Undika\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Auth::provider('civitas_undika', function ($app, array $config) {
            return $app->make(UserProvider::class, $config);
        });
    }
}
