<?php

namespace Undika\Auth\User;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

abstract class Civitas extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable;

    protected $keyType = 'string';
    public $timestamps = false;

    public function can($abilities, $arguments = [])
    {
        return Gate::forUser($this)->check($abilities, $arguments);
    }

    abstract public function scopeWherePin($query, $pin);
}
