<?php

namespace Undika\Auth\User;

class Karyawan extends Civitas
{
    protected $table = 'v_karyawan';
    protected $primaryKey = 'nik';

    public function scopeWherePin($query, $pin)
    {
        $key = $this->getKeyName();

        return $query->whereRaw("pass_kar_ok($key, ?) = 'TRUE'", $pin);
    }
}
