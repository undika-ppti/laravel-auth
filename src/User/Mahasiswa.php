<?php

namespace Undika\Auth\User;

class Mahasiswa extends Civitas
{
    protected $table = 'v_mhs';
    protected $primaryKey = 'nim';

    public function scopeWherePin($query, $pin)
    {
        $key = $this->getKeyName();

        return $query->whereRaw("pass_mhs_ok($key, ?) = 'TRUE'", $pin);
    }
}
