<?php

namespace Undika\Auth;

use Illuminate\Auth\EloquentUserProvider as UserProviderContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Str;

class UserProvider extends UserProviderContract
{
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

        $query = $this->createModel()->newQuery();

        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'pin')) {
                continue;
            }

            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    public function validateCredentials(AuthenticatableContract $user, array $credentials)
    {
        $pin = $credentials['pin'];
        $identifier = $user->getAuthIdentifier();

        return $this->createModel()->wherePin($pin)->find($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {
        // NOTE: DISABLED BECAUSE NOT SUPPORTED BY CURRENT DATA STRUCTURE
        return $this->retrieveById($identifier);
    }
}
